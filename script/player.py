import numpy as np
import cv2
import sys
import os
import datetime
import queue
import threading
import time

from PIL import Image, ImageFont, ImageDraw


class Player:

    def __init__(self, video, label=""):
        if label == "":
            sys.stderr.write("please enter the correct string.\n")
            sys.exit(1)

        self.video_feed = video
        self.text = label
        self.is_running = None
        self.threshold = 0.5

        self.bShow = True

        self.init_config()

        # thread functions
        self.cap_thread = None
        self.mask_thread = None

        sys.setrecursionlimit(10000)

    def init_config(self):

        # setup capture
        cap = cv2.VideoCapture(self.video_feed)
        if cap.isOpened():
            sys.stdout.write("success to conntect with {}\n".format(self.video_feed))
        else:
            sys.stderr.write("failed to conntect with {}\n".format(self.video_feed))
            sys.exit(1)

        fps = cap.get(cv2.CAP_PROP_FPS)
        width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
        height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)

        queue_len = 20  # for the discrete detection

        pos = cap.get(cv2.CAP_PROP_POS_FRAMES)
        cap.set(cv2.CAP_PROP_POS_FRAMES, pos + 700)

        self.cap = {
            'cap': cap,
            'fps': fps,
            'size': (width, height),
            'frame_queue': queue.Queue(),
            'mask_queue': queue.Queue(),
            'state': [],
            'queue_len': queue_len
        }

        margin = 1
        desired_w = int((self.cap['size'][0] / 1280 * 108) / 8 * len(self.text) / 2)
        desired_h = int(self.cap['size'][1] / 720 * 18 / 2)

        # load the font
        font = ImageFont.truetype("../font/protest.ttf", 18)
        (textSize_w, textSize_h) = font.getsize(self.text)

        # creat the text image(PIL) with text string
        pil_img = Image.new("RGB", [textSize_w + 2 * margin, textSize_h + 2 * margin])
        draw = ImageDraw.Draw(pil_img)
        draw.text((margin, margin), self.text, font=font)
        cv_img = cv2.cvtColor(np.array(pil_img), cv2.COLOR_BGR2GRAY)

        self.text_img = cv2.resize(cv_img, (desired_w + 2 * margin, desired_h + 2 * margin))
        self.text_h, self.text_w = self.text_img.shape[:2]

        if self.bShow:
            cv2.imshow("text", self.text_img)
            cv2.waitKey(1)

    def capture_frame(self):
        while self.is_running:
            if self.cap['frame_queue'].qsize() > 10:
                continue

            success, frame = self.cap['cap'].read()
            if not success:
                sys.stderr.write('cannot read frame\n')
                self.is_running = False
                sys.exit(1)

            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            gray = cv2.resize(gray, (640, 360))
            # self.cap['frame_queue'].put(frame)
            self.cap['frame_queue'].put(gray)

    def mask_frame(self):
        while self.is_running:
            try:
                gray = self.cap['frame_queue'].get(True, 1)
                # frame = self.cap['frame_queue'].get(True, 1)
                # gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

                res = cv2.matchTemplate(gray, self.text_img, cv2.TM_CCOEFF_NORMED)

                location = np.where(res >= self.threshold)

                for pt in zip(*location[::-1]):
                    cv2.rectangle(gray, pt, (pt[0] + self.text_w, pt[1] + self.text_h), (0, 0, 255), 1)

                if self.bShow:
                    cv2.imshow('frame', gray)
                    cv2.waitKey(1)
                    print(self.cap['frame_queue'].qsize())
                    # cv2.waitKey(1)
            
            except queue.Empty:
                pass

    def run(self):

        if self.cap:
            self.is_running = True

            self.cap_thread = threading.Thread(target=self.capture_frame)
            self.mask_thread = threading.Thread(target=self.mask_frame)

            self.cap_thread.start()
            self.mask_thread.start()

    def quit(self):

        self.is_running = False
        if self.cap_thread is not None and self.cap_thread.isAlive():
            self.cap_thread.join()
            self.cap_thread = None

        if self.mask_thread is not None and self.mask_thread.isAlive():
            self.mask_thread.join()
            self.mask_thread = None

if __name__ == '__main__':

    video_path = "../video/Playing.mp4"
    text_string = "0190C0F8"

    player = Player(video_path, text_string)
    sys.stdout.write(">>>lunched.\n")
    sys.stdout.write("input video: {}\n".format(video_path))
    sys.stdout.write("text string: {}\n".format(text_string))

    player.run()
    # while player.is_running:
    time.sleep(50)

    player.quit()
    sys.stdout.write(">>>finished.\n")
